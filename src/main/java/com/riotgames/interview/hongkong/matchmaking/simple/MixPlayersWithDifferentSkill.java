package com.riotgames.interview.hongkong.matchmaking.simple;

import com.riotgames.interview.hongkong.matchmaking.Match;
import com.riotgames.interview.hongkong.matchmaking.Player;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.PlayerRanking;

import java.util.*;

public class MixPlayersWithDifferentSkill implements PlayerPairingEngine {

    @Override
    public Match matchAvailablePlayers(int playersPerTeam, Map<PlayerRanking, LinkedList<Player>> playersByRatio, Comparator<Set<Player>> teamComparator) {
        Set<Player> team1 = new HashSet<>();
        Set<Player> team2 = new HashSet<>();

        for(int i = 0; i < playersPerTeam; i ++) {
            // pick player you want to match with others
            switch(teamComparator.compare(team1, team2)){
                // team1 is weaker
                case -1: team1.add(findBestPlayer(playersByRatio));
                    team2.add(findBalancingPlayer(playersByRatio));
                    break;
                // teams are balanced
                case 0 : team1.add(findBestPlayer(playersByRatio));
                    team2.add(findBestPlayer(playersByRatio));
                    break;
                // team2 is weaker
                case 1: team1.add(findBalancingPlayer(playersByRatio));
                    team2.add(findBestPlayer(playersByRatio));
                    break;
                default:
                    throw new RuntimeException("Unexpected comparison result therefore stopping");
            }
        }
        return new Match(team1, team2);
    }

    private Player findBestPlayer(Map<PlayerRanking, LinkedList<Player>> playersByRatio){
        if(!playersByRatio.get(PlayerRanking.High).isEmpty())
            return playersByRatio.get(PlayerRanking.High).pop();
        if(!playersByRatio.get(PlayerRanking.Medium).isEmpty())
            return playersByRatio.get(PlayerRanking.Medium).pop();
        if(!playersByRatio.get(PlayerRanking.Low).isEmpty())
            return playersByRatio.get(PlayerRanking.Low).pop();

        throw new RuntimeException("Shouldn't have been called if not sufficient players available");
    }

    private Player findBalancingPlayer(Map<PlayerRanking, LinkedList<Player>> playersByRatio){
        if(!playersByRatio.get(PlayerRanking.Low).isEmpty())
            return playersByRatio.get(PlayerRanking.Low).pop();
        if(!playersByRatio.get(PlayerRanking.Medium).isEmpty())
            return playersByRatio.get(PlayerRanking.Medium).pop();
        if(!playersByRatio.get(PlayerRanking.High).isEmpty())
            return playersByRatio.get(PlayerRanking.High).pop();

        throw new RuntimeException("Shouldn't have been called if not sufficient players available");
    }


}
