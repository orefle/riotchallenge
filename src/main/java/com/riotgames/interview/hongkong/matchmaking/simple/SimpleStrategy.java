package com.riotgames.interview.hongkong.matchmaking.simple;

import com.riotgames.interview.hongkong.matchmaking.Match;
import com.riotgames.interview.hongkong.matchmaking.Player;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.PlayerRanking;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.RatingCalculator;

import java.util.*;

public class SimpleStrategy implements Strategy {
    private final Map<PlayerRanking, LinkedList<Player>> playersByRatio = new HashMap<>();
    private Comparator<Set<Player>> teamComparator = null;

    private List<PlayerPairingEngine> playerPairingEngines;
    private RatingCalculator calculator;


    public SimpleStrategy(){
        playersByRatio.put(PlayerRanking.High, new LinkedList<>());
        playersByRatio.put(PlayerRanking.Medium, new LinkedList<>());
        playersByRatio.put(PlayerRanking.Low, new LinkedList<>());
    }

    @Override
    public Match matchAvailablePlayers(int playersPerTeam) {
        for(PlayerPairingEngine builder : playerPairingEngines) {
            Match match = builder.matchAvailablePlayers(playersPerTeam, playersByRatio, teamComparator);
            if (match != null)
                return match;
        }
        return null;
    }

    @Override
    public int getAvailablePlayers() {
        return playersByRatio.values().stream().mapToInt(LinkedList::size).sum();
    }

    @Override
    public void addPlayer(Player player) {
        PlayerRanking ratio = calculator.calculateRating(player);
        playersByRatio.get(ratio).push(player);
    }

    public void setRatingCalculator(RatingCalculator calculator){
        this.calculator = calculator;
        this.teamComparator = Comparator.comparing(x -> {
            OptionalDouble average = x.stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average();
            double result = 0;
            if(average.isPresent()) {
                result = average.getAsDouble();
            }
            return result;
        });
    }

    public void setPlayerPairingEngines(List<PlayerPairingEngine> playerPairingEngines) {
        this.playerPairingEngines = playerPairingEngines;
    }
}
