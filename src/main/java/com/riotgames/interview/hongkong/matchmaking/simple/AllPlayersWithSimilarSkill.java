package com.riotgames.interview.hongkong.matchmaking.simple;

import com.riotgames.interview.hongkong.matchmaking.Match;
import com.riotgames.interview.hongkong.matchmaking.Player;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.PlayerRanking;

import java.util.*;

public class AllPlayersWithSimilarSkill implements PlayerPairingEngine {


    @Override
    public Match matchAvailablePlayers(int playersPerTeam, Map<PlayerRanking, LinkedList<Player>> playersByRatio, Comparator<Set<Player>> teamComparator) {
        Set<Player> team1 = new HashSet<>();
        Set<Player> team2 = new HashSet<>();

        // if there are sufficient players in one similar category we let equal players play with each other
        Optional<LinkedList<Player>> bucketThatHasEnoughPlayers = playersByRatio.values().stream().filter(players -> players.size() >= playersPerTeam * 2).findFirst();
        if(!bucketThatHasEnoughPlayers.isPresent())
            return null;

        LinkedList<Player> availablePlayers = bucketThatHasEnoughPlayers.get();
        for(int i = 0 ; i < playersPerTeam * 2; i++ ){
            Player pickedPlayer = availablePlayers.pop();
            if(i %2 == 0){
                team1.add(pickedPlayer);
            }else{
                team2.add(pickedPlayer);
            }
        }

        if(team1.isEmpty() || team2.isEmpty()){
            return null;
        }else {
            return new Match(team1, team2);
        }
    }
}
