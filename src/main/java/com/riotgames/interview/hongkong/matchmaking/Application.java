package com.riotgames.interview.hongkong.matchmaking;

import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.ByKDAndWLCalculator;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.ByWinLossCalculator;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.RatingCalculator;
import com.riotgames.interview.hongkong.matchmaking.simple.AllPlayersWithSimilarSkill;
import com.riotgames.interview.hongkong.matchmaking.simple.SimpleStrategy;
import com.riotgames.interview.hongkong.matchmaking.simple.MixPlayersWithDifferentSkill;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class Application {
    private void startPairingPlayersByWL(){
        // specify how we rank players, options are either by Win/Loss, Kills/Deaths or a combination of these two
        RatingCalculator calculator = new ByWinLossCalculator();

        // setup match making strategy with a default strategy and an underlying team builder with a basic algorithm to match players for teams
        // of similar basic skill ranking
        Matchmaker matchmaker = new MatchMakerBuilder()
                .withStrategy(new SimpleStrategy())
                .addTeamBuilder(new AllPlayersWithSimilarSkill())
                .addTeamBuilder(new MixPlayersWithDifferentSkill())
                .withRatingCalculator(calculator)
                .build();


        // start player registration
        final LinkedList<Player> samplePlayers = new LinkedList<>(SampleData.getPlayers());

        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
        service.scheduleAtFixedRate(() -> {
                for(int i = 0; i< 10; i++){
                    matchmaker.enterMatchmaking(samplePlayers.pop());
                }
                },0,1, TimeUnit.SECONDS);

        // pick 30 random matches with either 3 or 5 people
        for(int i = 0; i < 30; i++){
            if(i%2 == 0){
                Match match = matchmaker.findMatch(3);
                System.out.println("Found Match:" + match);
                System.out.println("Team1 Strength:" + match.getTeam1().stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average().getAsDouble() + "/"
                        + "Team2 Strength:" + match.getTeam2().stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average().getAsDouble());
            }else {
                Match match = matchmaker.findMatch(5);
                System.out.println("Found Match:" + match);
                System.out.println("Team1 Strength:" + match.getTeam1().stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average().getAsDouble() + "/"
                        + "Team2 Strength:" + match.getTeam2().stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average().getAsDouble());
            }
        }
        service.shutdown();
    }

    private void sampleForPairingFirstTenPlayersByKD(){
        // specify how we rank players, options are either by Win/Loss, Kills/Deaths or a combination of these two
        RatingCalculator calculator = new ByWinLossCalculator();

        // setup match making strategy with a default strategy and an underlying team builder with a basic algorithm to match players for teams
        // of similar basic skill ranking
        Matchmaker matchmaker = new MatchMakerBuilder()
                .withStrategy(new SimpleStrategy())
                .addTeamBuilder(new AllPlayersWithSimilarSkill())
                .addTeamBuilder(new MixPlayersWithDifferentSkill())
                .withRatingCalculator(calculator)
                .build();

        // start player registration
        List<Player> samplePlayers = SampleData.getPlayers();
        samplePlayers.stream().limit(10).forEach(matchmaker::enterMatchmaking);

        Match match = matchmaker.findMatch(3);
        System.out.println("Found Match:" + match);
        System.out.println("Team1 Strength:" + match.getTeam1().stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average().getAsDouble() + "/"
                + "Team2 Strength:" + match.getTeam2().stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average().getAsDouble());


    }

    private void startPairingPlayersByWLAndKD() {
        // specify how we rank players, options are either by Win/Loss, Kills/Deaths or a combination of these two
        RatingCalculator calculator = new ByKDAndWLCalculator();

        // setup match making strategy with a default strategy and an underlying team builder with a basic algorithm to match players for teams
        // of similar basic skill ranking
        Matchmaker matchmaker = new MatchMakerBuilder()
                .withStrategy(new SimpleStrategy())
                .addTeamBuilder(new AllPlayersWithSimilarSkill())
                .addTeamBuilder(new MixPlayersWithDifferentSkill())
                .withRatingCalculator(calculator)
                .build();

        // start player registration
        List<Player> samplePlayers = SampleData.getPlayers();
        samplePlayers.stream().limit(10).forEach(matchmaker::enterMatchmaking);

        Match match = matchmaker.findMatch(3);
        System.out.println("Found Match:" + match);
        System.out.println("Team1 Strength:" + match.getTeam1().stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average().getAsDouble() + "/"
                + "Team2 Strength:" + match.getTeam2().stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average().getAsDouble());
    }

    public static void main(String[] args) {
        Application application = new Application();
        if(args.length==0) {
            application.startPairingPlayersByWL();
        }else if(args.length ==1){
            switch(args[0]){
                case "WL": application.startPairingPlayersByWL();
                    break;
                case "KD": application.sampleForPairingFirstTenPlayersByKD();
                    break;
                case "ALL" : application.startPairingPlayersByWLAndKD();
                    break;
                default: throw new RuntimeException("Unknown option");
            }
        }
    }

}

