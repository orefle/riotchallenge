package com.riotgames.interview.hongkong.matchmaking.ratingCalculator;

import com.riotgames.interview.hongkong.matchmaking.Player;

public class ByKillDeathRatio implements RatingCalculator {
    @Override
    public PlayerRanking calculateRating(Player player) {
        if(player.getKillDeathRatio() >= 2){
            return PlayerRanking.High;
        }else if (player.getKillDeathRatio() >= 0.7d){
            return PlayerRanking.Medium;
        }else {
            return PlayerRanking.Low;
        }
    }
}
