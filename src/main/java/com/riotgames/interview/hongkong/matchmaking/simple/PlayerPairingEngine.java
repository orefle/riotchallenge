package com.riotgames.interview.hongkong.matchmaking.simple;

import com.riotgames.interview.hongkong.matchmaking.Match;
import com.riotgames.interview.hongkong.matchmaking.Player;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.PlayerRanking;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public interface PlayerPairingEngine {
    Match matchAvailablePlayers(int playersPerTeam, Map<PlayerRanking, LinkedList<Player>> playersByRatio, Comparator<Set<Player>> teamComparator);
}
