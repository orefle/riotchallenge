package com.riotgames.interview.hongkong.matchmaking.ratingCalculator;

import java.util.Arrays;
import java.util.Optional;

public enum PlayerRanking {

    High(3), Medium(2), Low(1);

    private final int ratingValue;

    private PlayerRanking(int ratingValue){
        this.ratingValue = ratingValue;
    }

    public int getRatingValue() {
        return ratingValue;
    }

    public static PlayerRanking getCombinedRanking(PlayerRanking left, PlayerRanking right){
        int ranking = (left.getRatingValue() + right.getRatingValue()) / 2;
        Optional<PlayerRanking> first = Arrays.stream(PlayerRanking.values())
                .filter(x -> x.getRatingValue() == ranking)
                .findFirst();
        if(first.isPresent()){
            return first.get();
        }
        throw new RuntimeException("Can't combine given player rankings");

    }
}
