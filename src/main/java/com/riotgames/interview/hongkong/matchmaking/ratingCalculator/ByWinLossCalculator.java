package com.riotgames.interview.hongkong.matchmaking.ratingCalculator;

import com.riotgames.interview.hongkong.matchmaking.Player;

public class ByWinLossCalculator implements RatingCalculator {

    @Override
    public PlayerRanking calculateRating(Player player) {
        if(player.getWinPercentage() >= 0.7d){
            return PlayerRanking.High;
        }else if (player.getWinPercentage() >= 0.35d){
            return PlayerRanking.Medium;
        }else {
            return PlayerRanking.Low;
        }
    }
}
