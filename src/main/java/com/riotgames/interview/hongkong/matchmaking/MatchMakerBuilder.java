package com.riotgames.interview.hongkong.matchmaking;

import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.RatingCalculator;
import com.riotgames.interview.hongkong.matchmaking.simple.Strategy;
import com.riotgames.interview.hongkong.matchmaking.simple.PlayerPairingEngine;

import java.util.ArrayList;
import java.util.List;

public class MatchMakerBuilder {

    private Strategy strategy;
    private final List<PlayerPairingEngine> playerPairingEngines = new ArrayList<>();
    private RatingCalculator ratingCalculator;

    public MatchMakerBuilder withStrategy(Strategy strategy){
        this.strategy = strategy;
        return this;
    }

    public Matchmaker build(){
        MatchmakerImpl matchmaker = new MatchmakerImpl();
        matchmaker.setMatchingStrategy(strategy);
        strategy.setRatingCalculator(ratingCalculator);
        strategy.setPlayerPairingEngines(playerPairingEngines);
        return matchmaker;
    }


    public MatchMakerBuilder addTeamBuilder(PlayerPairingEngine builder) {
        playerPairingEngines.add(builder);
        return this;
    }

    public MatchMakerBuilder withRatingCalculator(RatingCalculator ratingCalculator){
        this.ratingCalculator = ratingCalculator;
        return this;
    }
}
