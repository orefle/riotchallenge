package com.riotgames.interview.hongkong.matchmaking;

/**
 * <p>
 * Representation of a player.
 * </p>
 * <p>
 * As indicated in the challenge description, feel free to augment the Player
 * class in any way that you feel will improve your final matchmaking solution.
 * <strong>Do NOT remove the name, wins, or losses fields.</strong> Also note
 * that if you make any of these changes, you are responsible for updating the
 * {@link SampleData} such that it provides a useful data set to exercise your
 * solution.
 * </p>
 */
public class Player {

    private final String name;
    private final long wins;
    private final long losses;
    private final int kills;
    private final int deaths;

    public Player(String name, long wins, long losses, int kills, int deaths) {
        this.name = name;
        this.wins = wins;
        this.losses = losses;
        this.kills = kills;
        this.deaths = deaths;
    }

    public String getName() {
        return name;
    }

    public long getWins() {
        return wins;
    }

    public long getLosses() {
        return losses;
    }

    public double getWinPercentage(){
        return (double)wins / (wins + losses);
    }
    public double getKillDeathRatio() { return (double)kills / deaths; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (losses != player.losses) return false;
        if (wins != player.wins) return false;
        if (!name.equals(player.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (int) (wins ^ (wins >>> 32));
        result = 31 * result + (int) (losses ^ (losses >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + getName() + ", W/L:" + wins +"/" +  losses + ", K/D:" + kills+"/" +  deaths;
    }
}
