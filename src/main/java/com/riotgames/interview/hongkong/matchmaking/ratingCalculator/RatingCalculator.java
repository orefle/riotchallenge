package com.riotgames.interview.hongkong.matchmaking.ratingCalculator;

import com.riotgames.interview.hongkong.matchmaking.Player;

public interface RatingCalculator {
    PlayerRanking calculateRating(Player player);
}
