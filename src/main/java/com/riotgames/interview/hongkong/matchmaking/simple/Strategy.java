package com.riotgames.interview.hongkong.matchmaking.simple;

import com.riotgames.interview.hongkong.matchmaking.Match;
import com.riotgames.interview.hongkong.matchmaking.Player;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.RatingCalculator;

import java.util.List;

public interface Strategy {

    /**
     * @param playersPerTeam
     * @return match if possible. null if there is no sufficient match to build a fair game
     */
    Match matchAvailablePlayers(int playersPerTeam);

    int getAvailablePlayers();

    void addPlayer(Player player);

    void setRatingCalculator(RatingCalculator calculator);
    void setPlayerPairingEngines(List<PlayerPairingEngine> playerPairingEngines);
}
