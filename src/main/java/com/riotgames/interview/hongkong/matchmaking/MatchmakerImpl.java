package com.riotgames.interview.hongkong.matchmaking;

import com.riotgames.interview.hongkong.matchmaking.simple.Strategy;

/**
 * The matchmaking implementation that you will write.
 */
public class MatchmakerImpl implements Matchmaker {

    private Strategy matchingStrategy;
    protected MatchmakerImpl() {}

    public void setMatchingStrategy(Strategy matchingStrategy) {
        this.matchingStrategy = matchingStrategy;
    }


    public Match findMatch(int playersPerTeam) {
        if(playersPerTeam != 1 && playersPerTeam != 3 && playersPerTeam != 5)
            throw new RuntimeException("Only supports 1v1, 3v3 and 5v5 games!");

        while(matchingStrategy.getAvailablePlayers() < playersPerTeam * 2){
            // wait until more players are added
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                // do nothing just continue waiting
            }
        }

        return matchingStrategy.matchAvailablePlayers(playersPerTeam);
    }

    public void enterMatchmaking(Player player) {
        matchingStrategy.addPlayer(player);
    }

}
