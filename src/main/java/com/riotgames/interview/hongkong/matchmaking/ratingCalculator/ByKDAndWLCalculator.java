package com.riotgames.interview.hongkong.matchmaking.ratingCalculator;

import com.riotgames.interview.hongkong.matchmaking.Player;

public class ByKDAndWLCalculator implements RatingCalculator{
    private ByWinLossCalculator winLossCalculator = new ByWinLossCalculator();
    private ByKillDeathRatio killDeathRatio = new ByKillDeathRatio();

    @Override
    public PlayerRanking calculateRating(Player player) {
        return PlayerRanking.getCombinedRanking(winLossCalculator.calculateRating(player), killDeathRatio.calculateRating(player));
    }
}
