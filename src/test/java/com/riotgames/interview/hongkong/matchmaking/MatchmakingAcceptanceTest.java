package com.riotgames.interview.hongkong.matchmaking;

import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.ByKDAndWLCalculator;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.ByKillDeathRatio;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.ByWinLossCalculator;
import com.riotgames.interview.hongkong.matchmaking.simple.AllPlayersWithSimilarSkill;
import com.riotgames.interview.hongkong.matchmaking.simple.MixPlayersWithDifferentSkill;
import com.riotgames.interview.hongkong.matchmaking.simple.SimpleStrategy;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.*;

import static org.junit.Assert.*;

public class MatchmakingAcceptanceTest {
    private Matchmaker matchmaker;
    private List<Player> samplePlayers = SampleData.getPlayers();

    @Before
    public void setup(){
        matchmaker = new MatchMakerBuilder()
                .withStrategy(new SimpleStrategy())
                .addTeamBuilder(new AllPlayersWithSimilarSkill())
                .addTeamBuilder(new MixPlayersWithDifferentSkill())
                .withRatingCalculator(new ByWinLossCalculator())
                .build();
        samplePlayers.stream().limit(10).forEach(x -> matchmaker.enterMatchmaking(x));
    }

    @Test(expected = RuntimeException.class)
    public void throws_exception_if_non_supported_number_of_players_requested(){
        matchmaker.findMatch(13);
    }

    @Test
    public void only_supports_pair_of_1_3_or_5_players(){
        samplePlayers.stream().limit(10).forEach(x -> matchmaker.enterMatchmaking(x));

        matchmaker.findMatch(5);
        matchmaker.findMatch(3);
        matchmaker.findMatch(1);

        try {
            matchmaker.findMatch(4);
            fail();
        }catch(RuntimeException e){
            assertTrue(true);
        }
    }

    @Test
    public void returned_match_contains_expected_number_of_players(){
        samplePlayers.stream().limit(10).forEach(x -> matchmaker.enterMatchmaking(x));
        Match match = matchmaker.findMatch(1);

        assertNotNull(match.getTeam1());
        assertEquals(1, match.getTeam1().size());

        assertNotNull(match.getTeam2());
        assertEquals(1, match.getTeam2().size());
    }

    @Test
    public void waits_until_enough_players_are_available_before_pairing() throws Exception {
        List<Player> playersForThisTest = Arrays.asList(
                new Player("Test1", 1,0,1,1),
                new Player("Test2", 1,0,1,1),
                new Player("Test3", 1,0,1,1),
                new Player("Test4", 1,0,1,1),
                new Player("Test5", 1,0,1,1),
                new Player("Test6", 1,0,1,1)
        );
        Matchmaker localMatchMaker = new MatchMakerBuilder()
                .withStrategy(new SimpleStrategy())
                .addTeamBuilder(new AllPlayersWithSimilarSkill())
                .addTeamBuilder(new MixPlayersWithDifferentSkill())
                .withRatingCalculator(new ByWinLossCalculator())
                .build();

        CompletableFuture<Match> match = CompletableFuture.supplyAsync(() ->
        {
            localMatchMaker.enterMatchmaking(playersForThisTest.get(0));
            return localMatchMaker.findMatch(3);
        });


        Match foundMatch = null;
        for(int i = 1; i < 5; i++){
            localMatchMaker.enterMatchmaking(playersForThisTest.get(i));
            try {
                foundMatch = match.get(1, TimeUnit.SECONDS);
            }catch(TimeoutException e){
                assertNull(foundMatch);
            }
        }

        localMatchMaker.enterMatchmaking(playersForThisTest.get(5));
        try {
            foundMatch = match.get(1, TimeUnit.SECONDS);
            assertNotNull(foundMatch);
        }catch(TimeoutException e){
            fail();
        }
    }

    @Test
    public void player_rating_calculator_can_be_switched_out_if_needed(){
        List<Player> playersForThisTest = Arrays.asList(
                new Player("Test1", 10,0,10,1),
                new Player("Test2", 10,0,1,10),
                new Player("Test3", 0,10,10,1),
                new Player("Test4", 0,10,1,10),
                new Player("Test5", 0,10,10,1),
                new Player("Test6", 10,0,10,1)
        );

        ByKillDeathRatio byKillDeathRatio = new ByKillDeathRatio();
        Comparator<Set<Player>> teamComparator = Comparator.comparing(x -> {
            OptionalDouble average = x.stream().mapToDouble(player -> byKillDeathRatio.calculateRating(player).getRatingValue()).average();
            double result = 0;
            if (average.isPresent()) {
                result = average.getAsDouble();
            }
            return result;
        });

        Matchmaker localMatchMaker = new MatchMakerBuilder()
                .withStrategy(new SimpleStrategy())
                .addTeamBuilder(new AllPlayersWithSimilarSkill())
                .addTeamBuilder(new MixPlayersWithDifferentSkill())
                .withRatingCalculator(byKillDeathRatio)
                .build();

        playersForThisTest.forEach(player -> localMatchMaker.enterMatchmaking(player));

        Match match = localMatchMaker.findMatch(3);
        assertEquals(0, teamComparator.compare(match.getTeam1(), match.getTeam2()));
    }






}
