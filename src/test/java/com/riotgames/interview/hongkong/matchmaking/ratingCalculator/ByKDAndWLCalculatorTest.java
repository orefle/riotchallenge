package com.riotgames.interview.hongkong.matchmaking.ratingCalculator;

import com.riotgames.interview.hongkong.matchmaking.Player;
import org.junit.Test;

import static org.junit.Assert.*;

public class ByKDAndWLCalculatorTest {

    private ByKDAndWLCalculator ratingCalculator = new ByKDAndWLCalculator();

    @Test
    public void player_with_high_kd_and_low_wl_is_ranked_medium(){
        assertEquals(PlayerRanking.Medium, ratingCalculator.calculateRating(new Player("Test", 1, 10, 2, 1)));
    }

    @Test
    public void player_with_high_kd_and_high_wl_is_ranked_high(){
        assertEquals(PlayerRanking.High, ratingCalculator.calculateRating(new Player("Test",10,2,3,1)));
    }

    @Test
    public void player_with_high_wl_and_low_kd_is_ranked_medium(){
        assertEquals(PlayerRanking.Medium, ratingCalculator.calculateRating(new Player("Test",10,0,1,3)));
    }

    @Test
    public void player_with_medium_wl_and_low_kd_is_ranked_low(){
        assertEquals(PlayerRanking.Low, ratingCalculator.calculateRating(new Player("Test",1,1,1,3)));
    }
}