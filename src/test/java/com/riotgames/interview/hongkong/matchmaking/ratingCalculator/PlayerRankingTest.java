package com.riotgames.interview.hongkong.matchmaking.ratingCalculator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PlayerRankingTest{

    @Test
    public void high_and_low_return_medium(){
        assertEquals(PlayerRanking.Medium, PlayerRanking.getCombinedRanking(PlayerRanking.High,PlayerRanking.Low));
    }
    @Test
    public void high_and_medium_return_medium(){
        assertEquals(PlayerRanking.Medium, PlayerRanking.getCombinedRanking(PlayerRanking.High,PlayerRanking.Medium));
    }
    @Test
    public void medium_and_low_return_low(){
        assertEquals(PlayerRanking.Low, PlayerRanking.getCombinedRanking(PlayerRanking.Medium,PlayerRanking.Low));
    }
}