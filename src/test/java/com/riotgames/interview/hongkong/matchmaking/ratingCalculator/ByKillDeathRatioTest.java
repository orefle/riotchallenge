package com.riotgames.interview.hongkong.matchmaking.ratingCalculator;

import com.riotgames.interview.hongkong.matchmaking.Player;
import org.junit.Test;

import static org.junit.Assert.*;

public class ByKillDeathRatioTest {
    private ByKillDeathRatio ratingCalculator = new ByKillDeathRatio();

    @Test
    public void player_with_kd_of_2_to_1_is_ranked_high(){
         assertEquals(PlayerRanking.High, ratingCalculator.calculateRating(new Player("Test", 0, 0, 2, 1)));
    }

    @Test
    public void player_with_kd_of_1_to_1_is_ranked_medium(){
        assertEquals(PlayerRanking.Medium, ratingCalculator.calculateRating(new Player("Test",0,0,1,1)));
    }

    @Test
    public void player_with_kd_of_less_than_1_to_3_is_ranked_low(){
        assertEquals(PlayerRanking.Low, ratingCalculator.calculateRating(new Player("Test",0,0,1,3)));
    }
}