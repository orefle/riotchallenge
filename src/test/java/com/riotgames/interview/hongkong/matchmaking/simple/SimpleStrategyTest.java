package com.riotgames.interview.hongkong.matchmaking.simple;

import com.google.common.collect.ImmutableSet;
import com.riotgames.interview.hongkong.matchmaking.Match;
import com.riotgames.interview.hongkong.matchmaking.Player;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.ByWinLossCalculator;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.*;

public class SimpleStrategyTest {

    private SimpleStrategy strategy;

    @Before
    public void setup(){
        strategy = new SimpleStrategy();
        strategy.setRatingCalculator(new ByWinLossCalculator());
        strategy.setPlayerPairingEngines(Arrays.asList(new AllPlayersWithSimilarSkill(), new MixPlayersWithDifferentSkill()));
    }
    @Test
    public void creates_match_for_players_of_equal_high_level() throws Exception {
        Player player1 = new Player("Test1",3,1,1,1);
        Player player2 = new Player("Test2",4,1,1,1);
        strategy.addPlayer(player1);
        strategy.addPlayer(player2);

        Match match = strategy.matchAvailablePlayers(1);
        assertEquals(1, match.getTeam1().size());
        assertEquals(1, match.getTeam2().size());

        if(match.getTeam1().contains(player1)){
            assertTrue(match.getTeam2().contains(player2));
        }else{
            assertTrue(match.getTeam1().contains(player2));
            assertTrue(match.getTeam2().contains(player1));
        }
    }

    @Test
    public void creates_match_for_players_of_equal_low_level() throws Exception {
        Player player1 = new Player("Test1",1,3,1,1);
        Player player2 = new Player("Test2",1,4,1,1);
        strategy.addPlayer(player1);
        strategy.addPlayer(player2);

        Match match = strategy.matchAvailablePlayers(1);
        assertEquals(1, match.getTeam1().size());
        assertEquals(1, match.getTeam2().size());

        if(match.getTeam1().contains(player1)){
            assertTrue(match.getTeam2().contains(player2));
        }else{
            assertTrue(match.getTeam1().contains(player2));
            assertTrue(match.getTeam2().contains(player1));
        }
    }

    @Test
    public void creates_match_for_players_of_equal_moderate_level() throws Exception {
        Player player1 = new Player("Test1",1,3,1,1);
        Player player2 = new Player("Test2",1,4,1,1);
        Player player3 = new Player("Test2",1,1,1,1);
        Player player4 = new Player("Test2",4,1,1,1);

        strategy.addPlayer(player1);
        strategy.addPlayer(player2);
        strategy.addPlayer(player3);
        strategy.addPlayer(player4);

        Match match = strategy.matchAvailablePlayers(1);
        assertEquals(1, match.getTeam1().size());
        assertEquals(1, match.getTeam2().size());

        if(match.getTeam1().contains(player1)){
            assertTrue(match.getTeam2().contains(player2));
        }else{
            assertTrue(match.getTeam1().contains(player2));
            assertTrue(match.getTeam2().contains(player1));
        }
    }

    @Test
    public void splits_strong_players_across_team() throws Exception{
        Player player1 = new Player("Test1",4,1,1,1);
        Player player2 = new Player("Test2",4,1,1,1);
        Player player3 = new Player("Test3",1,3,1,1);
        Player player4 = new Player("Test4",1,3,1,1);
        Player player5 = new Player("Test5",1,3,1,1);
        Player player6 = new Player("Test6",1,3,1,1);
        addPlayers(ImmutableSet.of(player1,player2,player3,player4,player5,player6));

        Match match = strategy.matchAvailablePlayers(3);
        assertNotNull(match);
        assertEquals(match.getTeam1().stream().mapToLong(x -> x.getWins()).sum(), match.getTeam2().stream().mapToLong(x -> x.getWins()).sum());
        assertEquals(match.getTeam1().stream().mapToLong(x -> x.getLosses()).sum(), match.getTeam2().stream().mapToLong(x -> x.getLosses()).sum());

    }

    private void addPlayers(Set<Player> players){
        players.forEach(player -> strategy.addPlayer(player));
    }






}