package com.riotgames.interview.hongkong.matchmaking.simple;

import com.riotgames.interview.hongkong.matchmaking.Match;
import com.riotgames.interview.hongkong.matchmaking.Player;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.ByWinLossCalculator;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.PlayerRanking;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.RatingCalculator;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MixPlayersWithDifferentSkillTest{
    private MixPlayersWithDifferentSkill matcher;
    private Comparator<Set<Player>> teamComparator = null;
    private RatingCalculator calculator = new ByWinLossCalculator();

    @Before
    public void setup(){
        matcher = new MixPlayersWithDifferentSkill();
        this.teamComparator = Comparator.comparing(x -> {
            OptionalDouble average = x.stream().mapToDouble(player -> calculator.calculateRating(player).getRatingValue()).average();
            double result = 0;
            if(average.isPresent()) {
                result = average.getAsDouble();
            }
            return result;
        });
    }

    @Test
    public void returns_balanced_group_if_two_very_good_ones_and_two_medium_ones_are_available(){
        Player high1 = new Player("High1", 3,1,1,1);
        Player high2 = new Player("High2", 3,1,1,1);
        Player medium1 = new Player("Medium1", 1,1,1,1);
        Player medium2 = new Player("Medium2", 1,1,1,1);


        Match match = matcher.matchAvailablePlayers(2, createPlayerMap(high1, high2, medium1, medium2), teamComparator);
        assertNotNull(match);
        assertEquals(4, match.getTeam1().stream().mapToLong(x -> x.getWins()).sum());
        assertEquals(4, match.getTeam2().stream().mapToLong(x -> x.getWins()).sum());
        assertEquals(2, match.getTeam1().stream().mapToLong(x -> x.getLosses()).sum());
        assertEquals(2, match.getTeam2().stream().mapToLong(x -> x.getLosses()).sum());
    }

    @Test
    public void balances_out_if_only_one_good_player_available_but_also_one_bad(){
        Player high1 = new Player("High1", 3,1,1,1);
        Player low1 = new Player("Low1", 1,3,1,1);
        Player medium1 = new Player("Medium1", 1,1,1,1);
        Player medium2 = new Player("Medium2", 1,1,1,1);
        Player medium3 = new Player("Medium3", 1,1,1,1);


        Match match = matcher.matchAvailablePlayers(2, createPlayerMap(high1, low1, medium1, medium2,medium3), teamComparator);
        assertNotNull(match);
        assertEquals(0, teamComparator.compare(match.getTeam1(), match.getTeam2()));
    }



    private Map<PlayerRanking, LinkedList<Player>> createPlayerMap(Player... players){
        Map<PlayerRanking, LinkedList<Player>> result = new HashMap<>();
        Arrays.stream(players).forEach(player -> {
            PlayerRanking playerBucket = calculator.calculateRating(player);
            LinkedList<Player> list = result.getOrDefault(playerBucket, new LinkedList<>());
            list.push(player);
            result.put(playerBucket, list);
        });

        return result;
    }
}