package com.riotgames.interview.hongkong.matchmaking;

import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.ByKDAndWLCalculatorTest;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.ByKillDeathRatioTest;
import com.riotgames.interview.hongkong.matchmaking.ratingCalculator.PlayerRankingTest;
import com.riotgames.interview.hongkong.matchmaking.simple.MixPlayersWithDifferentSkillTest;
import com.riotgames.interview.hongkong.matchmaking.simple.SimpleStrategyTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({MatchmakingAcceptanceTest.class,
        SimpleStrategyTest.class,
        ByKillDeathRatioTest.class,
        MixPlayersWithDifferentSkillTest.class,
        PlayerRankingTest.class,
        ByKDAndWLCalculatorTest.class})
public class AllTests {

}
