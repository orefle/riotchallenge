# Riot Coding Challenge
So, here’s the challenge. Pretend that we’ve created a new competitive online team-based game. Players of different skills levels will play the game, so we’ll need to match them in a way that keeps the game fun and fair. We want to match players based on their individual skill level and ensure teams are balanced. We're not expecting each matchup to be perfect (especially not at first), but we want the matchmaking system to eventually get smarter and accurately match players on teams.

Your challenge is to code a comprehensive matchmaking system. Your solution will repeatedly draw a set number of players from a larger pool and match them into teams. Players will enter the matchmaking process as solo participants, so your system should create balanced 3v3, 5v5, etc. teams. We’ve provided you with sample data for a pool of 200 players that includes their names, total wins and total losses. We’re happy to toss additional info into the matchmaking mix, so go ahead and invent a new data field and use that in yourcode if you’re feeling extra ambitious.

Use the project template in the attached .ZIP file as a starting point to create your matchmaking system. An engineer will take whatever you submit and test it, so make sure your solution will compile and run on any machine. Ideally, we want a flexible system that’ll allow us to edit the matchmaking rules and test out different strategies without extensive engineering efforts.


# Starting instructions
To start the application from a command line execute the following commands in the root directory of the project

1. `gradlew assemble`
2. `startApplication.bat`

The following Arguments can be provided to the bat file:

  * WL => Ranks players by Win/Loss (Uses full set of Sample data)
  * KD => Ranks players by Kill / Death (Only uses 10 players)
  * ALL => Combination of above. (Only uses 10 players)
  * No argument provided => Ranks players by Win/Loss (Uses full set of Sample data)